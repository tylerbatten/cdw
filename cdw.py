# population (gender + age) = 13
# ethnic origin = 3

def demo_percentage():
    prov_codes = {
    '10':'Newfoundland and Labrador',
    '11':'Prince Edward Island',
    '12':'Nova Scotia',
    '13':'New Brunswick',
    '24':'Quebec',
    '35':'Ontario',
    '46':'Manitoba',
    '47':'Saskatchewan',
    '48':'Alberta',
    '59':'British Columbia',
    '60':'Yukon',
    '61':'Northwest Territories',
    '62':'Nunavut'
    }
    provinces = [val for key,val in prov_codes.items()]
    province_choice = input(f'province (select one): {provinces} ')
    prov_code = [key for key,val in prov_codes.items() if val == province_choice ][0]
    areas = requests.get(f'https://www12.statcan.gc.ca/rest/census-recensement/CR2016Geo.json?lang=E&geos=CD&cpt={prov_code}')
    js = json.loads(areas.content.decode('utf-8')[2:])['DATA']
    county = [{county[4]:county[0]} for county in js]
    county_keys = []
    for i in county:
        for key,val in i.items():
            county_keys.append(key)
    county_choice = input(f'county (select one): {county_keys} ')
    for key_value in county:
        for key,value in key_value.items():
            try:
                if key == county_choice:
                    dguid = value
            except:
                print('Sorry, there is no data on this county yet.')
                exit()
    query = requests.get(f'https://www12.statcan.gc.ca/rest/census-recensement/CPR2016.json?topic=13&dguid={dguid}')
    j = json.loads(query.content.decode('utf-8')[2:])

    # gender makeup
    for i in j['DATA']:
        if '15 to 64 years' in i[10]:
            male = i[15]
            female = i[17]
            total = (i[13])
            male_perc = round(male/total*100, 1)
            fem_perc = round(female/total*100, 1)
            break # only need first one second one is perentage?
    gender = f'Of the {county_choice},{province_choice} population between the ages of 15 and 64, {male_perc}% are male and {fem_perc}% are female.'

    # age makeup
    age = []
    age_ranges = [
    '15 to 19 years',
    '20 to 24 years',
    '25 to 29 years',
    '30 to 34 years',
    '35 to 39 years',
    '40 to 44 years',
    '45 to 49 years',
    '50 to 54 years',
    '55 to 59 years',
    '60 to 64 years'
    ]
    age_counts = []
    count = 0
    age_rep = []
    for i in j['DATA']:
        try:
            if str(age_ranges[count]) in i[10]:
                age_counts.append({age_ranges[count]:round(i[13]/total*100,2)}) #T_DATA_DONNEE total of both genders --can also get total of each gender in each age group
                count += 1
        except:
            pass

    for i in age_counts:
        for key,val in i.items():
            age.append(f'{val}% are {key} old.')

    # Ethnicity makeup
    ethnicity = []
    eth_query = requests.get('https://www12.statcan.gc.ca/rest/census-recensement/CPR2016.json?dguid=2016A00031209&topic=3')
    j = json.loads(eth_query.content.decode('utf-8')[2:])
    for i in j['DATA']:
        if i[13]/total*100 >= 0.5 and i[9] == 3: #i[9] is INDENT_ID
            ethnicity.append(f'{i[10]} represent {round(i[13]/total*100,2)}% of the population living in {county_choice},{province_choice}')
    return (gender,age,'Ethnicity is reported only if the population is greater than 0.5%.',ethnicity)